import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LifeCycleHookComponent } from './pages/life-cycle-hook/life-cycle-hook.component';
import { TemplateReferenceComponent } from './pages/template-reference/template-reference.component';
import { PipesComponent } from './pages/pipes/pipes.component';
import { SharedModule } from '../shared/shared.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ParametersComponent } from './pages/parameters/parameters.component';
import { TutorialsComponent } from './pages/tutorials/tutorials.component';
import { RouterModule } from '@angular/router';
import { TemplateFormsComponent } from './pages/template-forms/template-forms.component';
import { ReactiveFormsComponent } from './pages/reactive-forms/reactive-forms.component';
import { ObservablesComponent } from './pages/observables/observables.component';

@NgModule({
  declarations: [
    LifeCycleHookComponent,
    TemplateReferenceComponent,
    PipesComponent,
    NotFoundComponent,
    ParametersComponent,
    TutorialsComponent,
    TemplateFormsComponent,
    ReactiveFormsComponent,
    ObservablesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    LifeCycleHookComponent,
    TemplateReferenceComponent,
    PipesComponent,
    NotFoundComponent,
    ParametersComponent,
    TutorialsComponent,
    TemplateFormsComponent,
    ReactiveFormsComponent,
    ObservablesComponent
  ]
})
export class CoreModule { }
