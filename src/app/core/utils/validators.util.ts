import { FormControl } from '@angular/forms'

export class ValidatorsUtil {
    static rangeStaticLength(control: FormControl) {
        if (!(control.value && control.value.length > 5 && control.value.length < 10)) {
            return { rangeStaticLength: 'Ingrese de 5 a 10 caracteres' }
        }
        
        return null
    }
    
    static rangeDinamicLength (min: number, max: number) {
        return (control: FormControl) => {
            if (!(control.value && control.value.length > min && control.value.length < max)) {
            return { rangeDinamicLength: `Ingrese de ${min} a ${max} caracteres` }
            }
        
            return null
        }
    }
      
}