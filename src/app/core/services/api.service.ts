import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos');
  }

  createTodo(body: { userId: number, title: string, completed: boolean }): Observable<Todo> {
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', body)
    /*
    return this.http.post<{ id: number}>('https://jsonplaceholder.typicode.com/todos', body)
    .pipe(
      map(res => {
        return {
          id: res.id,
          userId: body.userId,
          title: body.title,
          completed: body.completed
        }
      })
    )
    */
  }
  
  updateTodo(id: number, body: Todo): Observable<Todo> {
    return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, body);
  }

  deleteTodo(id: number): Observable<object> {
    return this.http.delete<object>(`https://jsonplaceholder.typicode.com/todos/${id}`);
  }

  getTasks(): Observable<string[]> {
    return of({ data: ['task 1', 'task 2'] })
    .pipe(
      map(res => res.data)
    )
  }

}
