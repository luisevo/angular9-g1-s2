import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {
  text = 'Curso de angular 9';
  list = [ 1, 2, 3, 4, 5 ];
  pi = 3.14159;
  price = 1000;
  percent = 0.01;
  student = {
    name: 'Julio',
    edad: 25
  }
  today = new Date();
  noValue: undefined;

  createFile: Promise<string>;

  constructor() {
    this.createFile = new Promise<string>((resolve, reject) => {
      setTimeout(() => {
        resolve('//files/file.tx');
      }, 2000);
    })
  }

  ngOnInit(): void {
  }

}
