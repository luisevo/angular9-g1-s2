import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { ValidatorsUtil } from '../../utils/validators.util';


@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {
  myForm: FormGroup;

  get email() {
    return this.myForm.get('email') as FormControl;
  }

  get emailInvalid() {
    return this.email.invalid && this.email.touched;
  }

  get passwordInvalid() {
    return this.myForm.get('password').invalid && this.myForm.get('password').touched;
  }

  get phones() {
    return this.myForm.get('phones') as FormArray;
  }

  constructor(private builder: FormBuilder) {
    this.myForm = this.builder.group({
      email: ['', [Validators.required, Validators.email, ValidatorsUtil.rangeDinamicLength(2, 6)]],
      password: ['', Validators.required],
      phones: this.builder.array([
        this.builder.control('')
      ])
    })

    // escuchar cambios en un control
    // this.email.valueChanges.subscribe( res => console.log(res))

    // escuchar cambios en un group
    // this.myForm.valueChanges.subscribe(res => console.log(res))
  }

  loadEmail() {
    /* actualiza todos los valores
    this.myForm.setValue({
      email: 'default@galaxy.com'
    })
    */
    this.myForm.patchValue({
      email: 'default@galaxy.com'
    })
  }

  clear() {
    this.myForm.reset();
  }

  addPhoneField() {
    this.phones.push(this.builder.control(''))
  }

  ngOnInit(): void {
  }

  save() {
    console.log(this.myForm);
  }

}
