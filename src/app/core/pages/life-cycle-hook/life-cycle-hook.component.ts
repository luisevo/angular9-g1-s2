import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-life-cycle-hook',
  templateUrl: './life-cycle-hook.component.html',
  styleUrls: ['./life-cycle-hook.component.scss']
})
export class LifeCycleHookComponent implements OnInit {
  cardTitle: string;
  showCard: boolean;

  constructor() {
    this.cardTitle = 'Mi Card';
    this.showCard = true;
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.cardTitle = 'Mi Card Editado';
      this.showCard = false;
    }, 2000)
  }

}
