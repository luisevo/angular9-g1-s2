import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-forms',
  templateUrl: './template-forms.component.html',
  styleUrls: ['./template-forms.component.scss']
})
export class TemplateFormsComponent implements OnInit {
  email = ''
  password = ''

  constructor() { }

  ngOnInit(): void {
  }

  save(myForm: NgForm) {
    /*
    Object.keys(myForm.controls).forEach(key => {
      myForm.controls[key].touched = true
    });
    */

    if (myForm.valid) {
      // ejecutar servicio
    }
  }
}
