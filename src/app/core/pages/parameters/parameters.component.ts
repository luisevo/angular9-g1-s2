import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss']
})
export class ParametersComponent implements OnInit {

  constructor(
    private activetedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.activetedRoute.snapshot.paramMap.get('id');
    console.log(id);
  }

}
