import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Todo } from '../../models/todo.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-observables',
  templateUrl: './observables.component.html',
  styleUrls: ['./observables.component.scss']
})
export class ObservablesComponent implements OnInit {
  todoForm: FormGroup;
  todos: Todo[] = [];

  constructor(
    private api: ApiService,
    private builder: FormBuilder
  ) {
    this.todoForm = this.builder.group({
      userId: 1,
      title: ['', Validators.required],
      completed: false
    })
  }

  ngOnInit(): void {
    // const publisher = of('Hola Mundo Reactivo')
    // const publisher = throwError('Error ocurrido en el servidor')
    // const publisher = of([ 'tarea 1', 'tarea 2' ])

    this.api.getTodos()
      .subscribe(
        todos => { 
          this.todos = todos;
        },
        err => { console.log('error', err) },
        () => { console.log('completado') },
      )

  }

  create() {
    this.api.createTodo(this.todoForm.value)
    .subscribe(
      todo => { 
        this.todos.unshift(todo)
      },
      err => { console.log('error', err) },
    )
  }
  
  update(index: number, todo: Todo) {
    const id = todo.id;
    const todoUpdated = { ...todo, completed: !todo.completed };

    this.api.updateTodo(id, todoUpdated)
    .subscribe(
      todo => { 
        this.todos[index] = todo;
      },
      err => { console.log('error', err) },
    )
  }

  delete(index: number, todo: Todo) {
    const id = todo.id;

    this.api.deleteTodo(id)
    .subscribe(
      todo => { 
        this.todos.splice(index, 1);
      },
      err => { console.log('error', err) },
    )
  }
}
