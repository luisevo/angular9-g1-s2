import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.scss']
})
export class TutorialsComponent implements OnInit {
  options = [
    { title: 'Pipes', url: 'pipes' },
    { title: 'Template Reference', url: 'template-reference' },
    { title: 'Life Cycle', url: 'life-cycle-hook' },
    { title: 'Template Forms', url: 'template-forms' },
    { title: 'Reactive Forms', url: 'reactive-forms' },
    { title: 'Observables', url: 'observables' },
  ]

  id = 12;

  constructor() { }

  ngOnInit(): void {
  }

}
