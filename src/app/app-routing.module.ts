import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateReferenceComponent } from './core/pages/template-reference/template-reference.component';
import { PipesComponent } from './core/pages/pipes/pipes.component';
import { LifeCycleHookComponent } from './core/pages/life-cycle-hook/life-cycle-hook.component';
import { NotFoundComponent } from './core/pages/not-found/not-found.component';
import { ParametersComponent } from './core/pages/parameters/parameters.component';
import { TutorialsComponent } from './core/pages/tutorials/tutorials.component';
import { TemplateFormsComponent } from './core/pages/template-forms/template-forms.component';
import { ReactiveFormsComponent } from './core/pages/reactive-forms/reactive-forms.component';
import { ObservablesComponent } from './core/pages/observables/observables.component';

const routes: Routes = [
  { path: '', redirectTo: 'tutorials/pipes', pathMatch: 'full' },
  {
    path: 'tutorials',
    component: TutorialsComponent,
    children: [
      { path: 'template-reference', component: TemplateReferenceComponent },
      { path: 'pipes', component: PipesComponent },
      { path: 'life-cycle-hook', component: LifeCycleHookComponent },
      { path: 'parameters', component: ParametersComponent },
      { path: 'parameters/:id', component: ParametersComponent },
      { path: 'template-forms', component: TemplateFormsComponent },
      { path: 'reactive-forms', component: ReactiveFormsComponent },
      { path: 'observables', component: ObservablesComponent },
    ]
  },
  {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
  },
  { path: '**', component: NotFoundComponent },

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
