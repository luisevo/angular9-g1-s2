import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-dropdown',
  templateUrl: './item-dropdown.component.html',
  styleUrls: ['./item-dropdown.component.scss']
})
export class ItemDropdownComponent implements OnInit {
  @Input() title: string;
  showContent = true;

  constructor() { }

  ngOnInit(): void {
  }

  open() {
    this.showContent = true;
  }

  close() {
    this.showContent = false;
  }

  toggle() {
    this.showContent = !this.showContent;
  }
}
