import { Component, OnInit, OnDestroy, OnChanges, DoCheck, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnDestroy, OnChanges, DoCheck {
  @Input() title: string;

  constructor() {
    console.log('[Card] constructor');
  }

  ngDoCheck(): void {
    console.info('[Card] ngDoCheck');
  }

  ngOnInit(): void {
    console.log('[Card] ngOnInit');
    // empezar escuchar eventos (suscribirnos)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('[Card] ngOnChanges', changes);
  }

  ngOnDestroy(): void {
    console.log('[Card] ngOnDestroy');
    // dejar empezar escuchar eventos (desuscribirnos)
  }

}
