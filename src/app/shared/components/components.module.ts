import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { DrawerComponent } from './drawer-container/drawer/drawer.component';
import { DrawerContainerComponent } from './drawer-container/drawer-container.component';
import { DrawerContentComponent } from './drawer-container/drawer-content/drawer-content.component';
import { ItemDropdownComponent } from './item-dropdown/item-dropdown.component';

@NgModule({
  declarations: [
    CardComponent,
    DrawerContainerComponent,
    DrawerContentComponent,
    DrawerComponent,
    ItemDropdownComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    DrawerContainerComponent,
    DrawerContentComponent,
    DrawerComponent,
    ItemDropdownComponent
  ]
})
export class ComponentsModule { }
