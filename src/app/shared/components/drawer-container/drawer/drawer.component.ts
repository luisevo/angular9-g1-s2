import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {
  private show = true;

  constructor(
    private el: ElementRef,
    private render: Renderer2
  ) { }

  ngOnInit(): void {
  }
  
  open() {
    this.show = true;
    // this.el.nativeElement.classList.remove('close')
    this.render.removeClass(this.el.nativeElement, 'close');
  }

  close() {
    this.show = false;
    // this.el.nativeElement.classList.add('close');
    this.render.addClass(this.el.nativeElement, 'close');
  }

  toggle() {
    this.show ? this.close() : this.open();
  }

}
